'''
input:the filename
output: If the file is not found or it can't be opened,it prints a message and returns an empty tuple
        Otherwise, it returns a list of tuples where the first element is the time statistics and the
        second element is the command.
'''

def parse_file(filename):
    rvalues=[]
    f = open(filename)
    for line in f:
        r = range(2,len(line),1)
        time = 0
        cmd = ""
        for i in r:
            if line[i]==':':
                time = line[2:i-1]
            if line[i]==';':
                cmd = line[i+1:len(line)-1]
                break
        cmd = cmd.split()[0]
        info = time,cmd
        rvalues.append(info)
    f.close()
    return rvalues

def test_parse_file():
    assert(parse_file("test.xt") == [('139039217', 'yum'), ('139039219', 'yum')])
    assert(parse_file("empty.txt")==[])
    try:
        parse_file("sfafs")
        assert False
    except IOError:
        assert True

